<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\EmployeeCrud;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EmployeeCrud */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Employees');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Employee'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?=  GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        // 'showHeader'=>false,
        //'showFooter'=>false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute'=>'id',
                //'header'=>'Employee Id'
                'label'=>'E_ID',
                'visible'=>false,
            ],

            'name',
            'email:email',
            'phoneno',
            [
                'attribute'=>'gender',
                'label'=>'Male\Female',
                /*'value'=>function($data,$key,$index){
                   //$newData=ucwords($data->gender).'==='.$index;
                    $newData=ucwords($data->gender);
                   return $newData;
                }*/
                'value'=>[EmployeeCrud::className(),'genderData'],
                'contentOptions'=>['style'=>'background-color:red;font-size:18px'],
                'filter'=>['male'=>'M','femal'=>'F']
            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'header'=>'Action',
                'headerOptions'=>['width'=>'90'],
                'template'=>' {update} {view} {delete}'

            ],

        ],
        // 'layout'=>"{pager}\n{items}\n{summary}"
        'rowOptions'=>function($data,$key,$index){
            $class='test';
            return ['key'=>$key,'index'=>$index,'class'=>$class];
        },
        //  'showOnEmpty'=>false
        //  'emptyCell'=>'NA'
    ]); ?>





</div>
