<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%employee}}".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property int $phoneno
 * @property string $gender
 */
class Employee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%employee}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'email', 'gender'], 'required'],
            [['id', 'phoneno'], 'integer'],
            [['name', 'email'], 'string', 'max' => 52],
            [['gender'], 'string', 'max' => 5],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'email' => Yii::t('app', 'Email'),
            'phoneno' => Yii::t('app', 'Phoneno'),
            'gender' => Yii::t('app', 'Gender'),
        ];
    }
}
