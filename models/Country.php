<?php

namespace app\models;

use Yii;
use yii\db\Query;

/**
 * This is the model class for table "{{%country}}".
 *
 * @property string $code
 * @property string $name
 * @property int $population
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name'], 'required'],
            [['population'], 'integer'],
            [['code'], 'string', 'max' => 2],
            [['name'], 'string', 'max' => 52],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'population' => Yii::t('app', 'Population'),
        ];
    }
    public function getData()
    {
        //insert///

       /* $sql=Yii::$app->db->createCommand()->insert('country',
        [
            'code'=>'AF',
            'name'=>'Africa',
            'population'=>20000,
            'ccode'=>13,
        ])->execute();*/

     // $lastId=Yii::$app->db->getLastInsertID();  //get last id
      //echo $lastId;


        //update//
     /*   $sql=Yii::$app->db->createCommand()->update('country',
            [
                'name'=>'African',
                'population'=>50000,
            ],array('code'=>'AF'))->execute();

     */

        //delete
       // $sql=Yii::$app->db->createCommand()->delete('country',array('code'=>'AF'))->execute();


        //select///

        $query=(new Query())
            ->select('country.*')
             ->from('country')
            ->where(['code'=>'TR'])
            ->all();
        echo '<pre>';print_r($query);
    }
}
