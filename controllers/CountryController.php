<?php

namespace app\controllers;

use Yii;
use app\Models\Country;
use app\Models\CountrySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Country models.
     * @return mixed
     */
    public function actionIndex()
    {
        /* $data = Country::find()->all();
         echo '<pre>';print_r($data);
        die;*/

        /////INSERT/////
      /*  $data=new Country();
        $data->code='TR';
        $data->name='Turkey';
        $data->population=2000;


        $data->save();*/

        /////UPDATE/////
        /* $data=Country::findOne('TR');
          $data->name='Turkeyie';
          $data->population=20000;

          $data->save();*/
/////////////////////////////////////////////////////////////////////////////////////////////////
        ////SELECT ////
        //$data = Country::find()->all();  //select all data

       // $data = Country::findAll(['code'=>'AU']);  //select  data with cond
       // echo '<pre>';print_r($data);
       /* $data = Country::find()->all();   //loop through values
        foreach($data as $val){
            echo $val->name.'<br/>';
        }
       ///////////////////////////////////////////////////////////////
        /* $data = Country::find()->asArray()->all();   //convert to array
        foreach($data as $val){
            echo $val['name'].'<br/>';
        }*/
        //////////////////////////////////////////////////////////////////////////////////////////////
       // $data = Country::find()->where(['code'=>'AU']);   //where condition
        //$data = Country::find()->where(['code'=>'AU','name'=>'Australia']);
        //$data = Country::find()->where(['code'=>['AU','TR']]);  //like IN
       // $data = Country::find()->where(['code'=>['AU','TR']])->groupBy('population');  //group by
       // echo  $data->createCommand()->getRawSql();  //see select * from tabname where query in raw form

      //  $data = Country::find()->where(['code'=>['AU','TR']])->groupBy('code')->orderBy('code')->all();  //get actual query values in where
       // foreach($data as $val){
         //   echo $val->code.'<br/>'.$val->name.'<br/>';
       // }

     ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////JOIN/////


        $searchModel = new CountrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Country model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Country model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Country();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->code]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Country model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->code]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Country model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Country model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Country the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Country::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
    public function actionQueryBuilder(){
        $data=new Country();
        echo $data->getData();
       // echo '<pre>';print_r($data);
        //die;
    }
}
